// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'iview/dist/styles/iview.css'
import {
  Button, Menu, Icon, MenuGroup, MenuItem, Submenu, Layout,
  Header, Content, Footer, Sider, Breadcrumb,BreadcrumbItem,Card,
  Tabs,TabPane
} from 'iview';

Vue.config.productionTip = false
Vue.component('Button', Button)
Vue.component('Menu', Menu);
Vue.component('Icon', Icon);
Vue.component('MenuGroup', MenuGroup);
Vue.component('MenuItem', MenuItem);
Vue.component('Submenu', Submenu);
Vue.component('Layout', Layout);
Vue.component('Header', Header);
Vue.component('Content', Content);
Vue.component('Footer', Footer);
Vue.component('Sider', Sider);
Vue.component('Breadcrumb', Breadcrumb);
Vue.component('BreadcrumbItem', BreadcrumbItem);
Vue.component('Card', Card);
Vue.component('Tabs', Tabs);
Vue.component('TabPane', TabPane);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
